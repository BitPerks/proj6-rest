"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import os
from webapi import api as b_api
from flask_restful import Api
import csv

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.controls
control_list = db.samples
method_list = {'get_data': b_api.b_Read,'replace_data':b_api.b_Create,'set_data':b_api.b_Create,'get_query':b_api.b_Delete}
b_api.db_set(control_list)

##############
#Cut/Paste Functions
##############

def csv_parse(file_name, fieldnames, input, *args):
    result = ""
    with open(file_name, "w") as close_text:
        data_writer = csv.DictWriter(close_text, fieldnames=fieldnames)
        data_writer.writeheader()
        if args:
            top = args[1]
            for x in range(control_list.count_documents({}, limit=top)):
                if str(b_api.b_Read.get(b_api.b_Read, x, input)):
                    data_writer.writerow(b_api.b_Read.get(b_api.b_Read, x, input))
        else:
            for x in range(control_list.count_documents({})):
                if str(b_api.b_Read.get(b_api.b_Read, x, input)):
                    data_writer.writerow(b_api.b_Read.get(b_api.b_Read, x, input))

    with open(file_name, 'r') as read_file:
        data_reader = csv.reader(read_file)
        for row in data_reader:
            result += str(row) + "\n "
        read_file.close
    os.remove(file_name)
    return result

def json_parse():
    pass

###
# Pages
###
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    flask.session['data_entries'] = 0
    return flask.render_template('calc.html')

#api.add_resource(b_api.b_Read, "/test")
#api.add_resource(b_api.b_Create, "/_create/<int:id>")

@app.route("/listAll")
@app.route("/listAll.json")
def list_all():
    data_list = ""
    if b_api.client.count_documents({}):
        for x in range(0, control_list.count_documents({})):
            data_list += str(b_api.b_Read.get(b_api.b_Read, x)) + "\n "
        return flask.render_template('display.html', db_data=data_list)
    else:
        return flask.render_template('calc.html')

@app.route("/listAll.csv")
def csv_list():
    fieldnames = ['close_time', 'id', 'open_time', 'km']
    top = request.args.get('top', type=int)
    input = {'_id': 0}
    file_name = 'open_file.csv'
    if top:
        if b_api.client.count_documents({}):
            result = csv_parse(file_name, fieldnames, input, top)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')
    else:
        if b_api.client.count_documents({}):
            result = csv_parse(file_name, fieldnames, input)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')

@app.route("/listOpenOnly.csv")
def csv_open():
    fieldnames = ['open_time']
    top = request.args.get('top', type=int)
    input = {'id': 0, 'close_time': 0, '_id': 0, 'km': 0}
    file_name = 'open_file.csv'
    if top:
        if b_api.client.count_documents({}):
            result = csv_parse(file_name, fieldnames, input, top)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')
    else:
        if b_api.client.count_documents({}):
            result = csv_parse(file_name, fieldnames, input)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')

@app.route("/listCloseOnly.csv")
def csv_close():
    fieldnames = ['close_time']
    top = request.args.get('top', type=int)
    input = {'id': 0, 'open_time': 0, '_id': 0, 'km': 0}
    file_name = 'close_file.csv'
    if top:
        if b_api.client.count_documents({}):
            result = csv_parse(file_name, fieldnames, input, top)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')
    else:
        if b_api.client.count_documents({}):
            result = csv_parse(file_name, fieldnames, input)
            return flask.render_template('display.html', db_data=result)
        else:
            return flask.render_template('calc.html')


@app.route("/listOpenOnly")
@app.route("/listOpenOnly.json")
def open_only():
    top = request.args.get('top', type=int)
    if top:
        data_list = {}
        if b_api.client.count_documents({}):
            for x in range(control_list.count_documents({}, limit=top)):
                input = {'id': 0, 'close_time': 0, '_id': 0, 'km': 0}
                data_list[x] = b_api.b_Read.get(b_api.b_Read, x, input)
                app.logger.debug(str(x))
            return flask.render_template('display.html', db_data=data_list)
        else:
            return flask.render_template('calc.html')
    else:
        data_list = {}
        if b_api.client.count_documents({}):
            for x in range(control_list.count_documents({})):
                data_list[x] = b_api.b_Read.get(b_api.b_Read, x, {'id': 0, 'close_time': 0, '_id': 0, 'km': 0})
                app.logger.debug(str(x))
            return flask.render_template('display.html', db_data=data_list)
        else:
            return flask.render_template('calc.html')

@app.route("/listCloseOnly")
@app.route("/listCloseOnly.json")
def close_only():
    top = request.args.get('top', type=int)
    if top:
        data_list = {}
        if b_api.client.count_documents({}):
            for x in range(control_list.count_documents({}, {'open_time': 0, 'id': 0, '_id': 0, 'km': 0}, limit=top)):
                data_list[x] = b_api.b_Read.get(b_api.b_Read, x)
                app.logger.debug(str(x))
            return flask.render_template('display.html', db_data=data_list)
        else:
            return flask.render_template('calc.html')
    else:
        data_list = {}
        if b_api.client.count_documents({}):
            for x in range(control_list.count_documents({})):
                data_list[x] = b_api.b_Read.get(b_api.b_Read, x, {'open_time': 0, 'id': 0, '_id': 0, 'km': 0})
                app.logger.debug(str(x))
            return flask.render_template('display.html', db_data=data_list)
        else:
            return flask.render_template('calc.html')


@app.route("/_parse_all")
def parse_all():
    data_list = ""
    if flask.session['data_entries']:
        for x in range(control_list.count_documents({})):
            data_list[x] = b_api.b_Read.get(b_api.b_Read, x)
            app.logger.debug(str(x))
    return jsonify(data_list)

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/brevet_times")
def brevet_page():
    app.logger.debug("Page for displaying values in mongoDB")
    #create list of values via mongoDB
    return #literal template for time display
            #How do I send list of data to html page? Variables of one list? While loop?

################
#
# AJAX DB handlers
#   for entering/returning data for mongoDB
#################
@app.route("/_display_values", methods=['GET'])
def _display_values():
    data_list = {}
    if b_api.client.count_documents({}):
        for x in range(0, control_list.count_documents({})):
            data_list[x] = b_api.b_Read.get(b_api.b_Read, x)
            app.logger.debug(str(x))
        return flask.render_template('display.html', db_data=data_list)
    else:
        return flask.render_template('calc.html')


@app.route("/_update_values", methods=['POST', 'PUT'])
def _update_values():
    #mongo add/append to database commands
    #create structure for distance, time, total distance, open, close
    #code below is copy of display code. Replace data setting with
    #mongo append and addition commands. Add a mongo initializer
    #for the database within the index page response

    app.logger.debug("Got a AJAX request")
    id_value = control_list.count_documents({})
    read_check = "call reference"
    km = str(request.form.get('km', type = int))
    open_time = request.form.get('brevet_open', type = str)
    close_time = request.form.get('brevet_close', type = str)
    parse_data = {'id': id_value, 'km': km, 'open_time': open_time, 'close_time': close_time}
    result = str(parse_data)
    #return str(parse_data)
    if open_time == "Proper Time" or open_time == "Positive Integer" or close_time == "Proper Time" or close_time =="Positive Integer":
        return flask.jsonify(result="improper fields in data, please properly fill them in.")
    if open_time and close_time:
        if control_list.find_one(parse_data) == parse_data:
            result = "Data already exists. Please enter new data"
        else:
            b_api.b_Create.post(b_api.b_Create, parse_data) #control_list.insert({km: km, open_time: open_time, close_time: close_time})
            app.logger.debug("got a db update: " + str(control_list.find_one(parse_data)))
            if b_api.b_Read.get(b_api.b_Read, id_value):
                flask.session['data_entries'] =+ 1
                result = "success"
            else:
                result = "fail"
    #remove data that possibly got inserted, display full database.
    return result



###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
        
   # Calculates open/close times from miles, using rules
   # described at https://rusa.org/octime_alg.html.
   # Expects one URL-encoded argument, the number of miles.
    
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', type = float)
    total_distance = request.args.get('brevet_distance', type = int)
    start_time = request.args.get('timestamp', type = str)
    start_time = arrow.get(start_time)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, total_distance, start_time.isoformat())
    close_time = acp_times.close_time(km, total_distance, start_time.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug = True)
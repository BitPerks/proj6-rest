# Laptop Service

import flask
from flask_restful import Resource, Api
from pymongo import MongoClient
import os
import config

# Instantiate the app
app = flask.Flask(__name__)
web_api = Api(app)
client = "empty"

def db_set(db_init):
    global client
    client = db_init

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }


class b_Sort(Resource):
    def get(self, req):
        pass


class b_Create(Resource):
    def post(self, b_data):
        if b_data:
            client.insert(b_data)

class b_Delete(Resource):
    def get(self):
        #add data delete?
        return{"Resource b_delete was called"}

class b_Read(Resource):
    def get(self, *args):
        if len(args) == 1:
            b_data_id = args[0]
            data = client.find_one({'id': b_data_id})
            if data:
                return str(data)
            else:
                return 0
            return{"Resource b_read was called"}
        elif len(args) == 2:
            b_data_id = args[0]
            query = args[1]
            data = client.find_one({'id': b_data_id}, query)
            if data:
                return data
            else:
                return 0

# Create routes
# Another way, without decorators
#Call these in brevets main code to use api
#api.add_resource(Laptop, '/')
#api.add_resource(b_create,'/_create')
#api.add_resource(b_delete,'/_delete')
#api.add_resource(b_read,'/_read')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)

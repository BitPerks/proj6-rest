# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## What is in this repository

I've created a basic api system and a more complicated routing code base for my brevet calculator from proj5. The api system is extremely simple and covers all of my DB input/outputs. New routes and code have been added to flask for maintaing specific calls and information.

## Author

Isaac Perks: iperks@uoregon.edu
Repo: UOCISS 322: proj6-rest
